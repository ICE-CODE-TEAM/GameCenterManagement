package Details;

import Data.LogTable;
import Data.Person;
import DataBase.DataBaseHepler;
import Utils.Utils;
import Utils.JalaliCalendar;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

public class detailController {
    @FXML
    ProgressBar pb;

    @FXML
    Label t1;

    @FXML
    Label d1;

    @FXML
    Label t2;

    @FXML
    Label d2;

    @FXML
    Label t3;

    @FXML
    Label d3;

    @FXML
    Label t4;

    @FXML
    Label d4;

    @FXML
    Label t5;

    @FXML
    Label d5;

    @FXML
    Label t6;

    @FXML
    Label d6;

    @FXML
    Label t7;

    @FXML
    Label d7;

    @FXML
    Label t8;

    @FXML
    Label d8;

    @FXML
    Label t9;

    @FXML
    Label d9;

    @FXML
    Label t10;

    @FXML
    Label d10;

    @FXML
    Label name;

    @FXML
    Label birthDate;

    @FXML
    Label joinDate;

    @FXML
    Label code;

    @FXML
    Label phone;

    @FXML
    Label idCode;

    @FXML
    Label fullTime;


    private Person person;

    private void setPb(Double d) {
        pb.setProgress(d);
    }

    public void setPerson(Person p) {
        person = p;

        name.setText(p.getName());
        birthDate.setText(p.getBirthDate());
        joinDate.setText(p.getJoinDate());
        idCode.setText(p.getIdCode());
        code.setText(String.valueOf(p.getCode()));
        phone.setText(p.getNumber());

        List<Label> times = new ArrayList<>(Arrays.asList(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10));
        List<Label> dates = new ArrayList<>(Arrays.asList(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10));

        List<LogTable> onLog = DataBaseHepler.getTenLastPersonPlayed(person);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        JalaliCalendar jalaliCalendar = new JalaliCalendar();

        if (!onLog.isEmpty()) {
            for (int i = 0; i < onLog.size(); i++) {
                LogTable log = onLog.get(i);
                Date date = new Date(log.getStartTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                dateFormat.setCalendar(jalaliCalendar);
                dates.get(i).setText(dateFormat.format(date.getTime()));
                Date time = new Date((log.getEndTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()) - log.getStartTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                times.get(i).setText(timeFormat.format(time));
            }
        }

        fullTime.setText(timeFormat.format(new Date(DataBaseHepler.getTotalTime(person))));
    }
//
//    @FXML
//    void initialize() {
//
//    }

    @FXML
    void close() {
        Utils.closeStage(pb, false);
    }


}
