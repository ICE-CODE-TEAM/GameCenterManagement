package Main;

import DataBase.DataBaseHepler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import static Utils.Resources.*;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        Font.loadFont(getClass().getResourceAsStream(BKOOD_FONT), BKOOD_FONT_SIZE);
        Font.loadFont(getClass().getResourceAsStream(BTITRBD_FONT), BTITRBD_FONT_SIZE);

        Parent root = FXMLLoader.load(getClass().getResource(LIST_FXML));
//        Parent root = FXMLLoader.load(getClass().getResource(FIRSTPAGE_FXML));
        primaryStage.setTitle(AppName_TITLE);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setScene(new Scene(root, SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
