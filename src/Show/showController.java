package Show;

import DataBase.DataBaseHepler;
import DeskStatus.deskStatusController;
import List.listController;
import Main.Main;
import Utils.Utils;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

import static DataBase.DataBaseHepler.checkPlayTableExistence;
import static Utils.Resources.*;

/**
 * Created by arm on 8/18/17.
 */
public class showController {
    @FXML
    Button _1;

    public void openDialog(ActionEvent event) {

        Button button = (Button) event.getSource();
        String btnId = Utils.getID(button.getId());
        if (checkPlayTableExistence(Integer.parseInt(btnId)))
            showCloseStage(event,btnId);
        else
            showFirstPlayerStage(event);

    }

    public void showCloseStage(ActionEvent event,String btnId) {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(DESKSTATUS_CLOSE_FXML));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        deskStatusController controller = loader.getController();
        controller.setmShowcontroller(this);
        controller.setStatus(btnId);
        deskStatusController.setId(((Button) (event.getSource())).getId());
        Stage stage = new Stage();

        Scene scene = new Scene(root, 500, 400);
        stage.setScene(scene);
        stage.show();
    }

    public void showFirstPlayerStage(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(DESKSTATUS_OPEN_FXML));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        deskStatusController controller = loader.getController();
        controller.setmShowcontroller(this);
        deskStatusController.setId(((Button) (event.getSource())).getId());
        controller.setmShowcontroller(this);
        Scene scene = new Scene(root, 500, 300);
        Stage stage = new Stage();
        stage.setTitle(DESKSTATUS_TITLE);
        stage.initOwner(_1.getScene().getWindow());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(scene);
        stage.show();
    }

    public void openList(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(LIST_FXML));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle(LIST_TITLE);
        listController controller = loader.getController();
        controller.load();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(_1.getScene().getWindow());
        Scene scene = new Scene(root, SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD);
        stage.setScene(scene);
        stage.show();
    }

    public void signUp(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(SIGNUP_FXML));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle(SIGNUP_TITLE);

        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(_1.getScene().getWindow());
        stage.initStyle(StageStyle.UNDECORATED);

        Scene scene = new Scene(root, 600, 400);
        stage.setScene(scene);
        stage.show();
    }

    public void close() {
        Utils.closeStage(_1,true);
    }

    public void setButtonStyle(boolean inUse, int tableNumber) {
        Button button = (Button) _1.getScene().lookup("#_" + tableNumber);

        String styleInuse = getClass().getResource("/Resources/css/inUseButton.css").toExternalForm();
        String styleFree = getClass().getResource("/Resources/css/freeUseButton.css").toExternalForm();

        if (inUse) {
            button.getStylesheets().remove(styleFree);
            button.getStylesheets().add(styleInuse);
        } else {
            button.getStylesheets().remove(styleInuse);
            button.getStylesheets().add(styleFree);
        }
    }

    public void checkTables() {
        for (int i = 1; i <= 12; i++) {
            if (DataBaseHepler.searchPlayTable(i) != null)
                setButtonStyle(true, i);
        }
    }

    @FXML
    void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                checkTables();
            }
        });
    }
}
