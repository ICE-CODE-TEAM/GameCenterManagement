package List;

import Data.Person;
import DataBase.DataBaseHepler;
import Details.detailController;
import Utils.Resources;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.IOException;
import java.util.List;

import static Utils.Resources.*;

/**
 * Created by arm on 8/26/17.
 */
public class listController {

    @FXML
    TableView<Person> persons;

    @FXML
    public void load() {
        ObservableList<Person> list = persons.getItems();
        list.clear();
        List<Person> p = DataBaseHepler.getPersons();
        list.addAll(p);

        persons.setRowFactory(new Callback<TableView<Person>, TableRow<Person>>() {
            @Override
            public TableRow<Person> call(TableView<Person> param) {

                TableRow<Person> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        showDetail(row.getItem());
                    }
                });
                return row;
            }
        });
    }

    public void showDetail(Person person){
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Resources.DETAIL_FXML));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setTitle(DETAIL_TITLE);
        detailController controller = loader.getController();
        controller.setPerson(person);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(persons.getScene().getWindow());
        Scene scene = new Scene(root, SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD);
        stage.setScene(scene);
        stage.show();
    }


    @FXML
    void initialize() {
        load();
    }
}