package DataBase;

import Data.LogTable;
import Data.Person;
import Data.PlayTable;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class DataBaseHepler {


    public static Person searchPersonID(int id) {
        EntityManager em = EntityFactory.getEntityManager();
        List<Person> results = em.createQuery("SELECT p FROM Person p WHERE p.id=:personId").setParameter("personId", id).getResultList();
        if (results.size() == 1)
            return results.get(0);
        return null;
    }

    public static PlayTable searchPlayTable(int tableNumber) {
        EntityManager em = EntityFactory.getEntityManager();
        List<PlayTable> results = em.createQuery("SELECT t FROM PlayTable t WHERE t.tableNumber=:tableNumber").setParameter("tableNumber", tableNumber).getResultList();
        if (results.size() == 1)
            return results.get(0);
        return null;
    }

    public static PlayTable deletePlayTable(int tableNumber) {
        EntityManager em = EntityFactory.getEntityManager();
        EntityTransaction tr = em.getTransaction();
        if (!tr.isActive())
            em.getTransaction().begin();

        PlayTable playTable = searchPlayTable(tableNumber);
        if (playTable != null) {
            em.remove(playTable);
            tr.commit();
            return playTable;
        }
        return null;
    }

    public static boolean checkPlayTableExistence(int tableNumber) {
        PlayTable t = DataBaseHepler.searchPlayTable(tableNumber);
        return (t != null);
    }

    public static boolean createLog(LogTable t) {
        EntityManager em = EntityFactory.getEntityManager();
        try {
            EntityTransaction tr = em.getTransaction();
            if (!tr.isActive())
                em.getTransaction().begin();
            em.persist(t);
            tr.commit();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static boolean createOnePersonPlayer(int userId, int tableNumber) {

        EntityManager em = EntityFactory.getEntityManager();
        List<Person> p = em.createQuery("SELECT p FROM Person p where p.code=:code")
                .setParameter("code", userId).getResultList();

        if (!p.isEmpty()) {
            LocalDateTime nowD = LocalDateTime.now();
            if (!em.getTransaction().isActive())
                em.getTransaction().begin();
            Person po = (Person) em.createQuery("SELECT p FROM Person p where p.code=:code")
                    .setParameter("code", userId).getSingleResult();
            PlayTable t = new PlayTable(po, nowD, tableNumber);
//            po.addTable(t);
            em.persist(t);
            em.getTransaction().commit();
            return true;
        } else
            return false;
    }

    public static boolean createTwoPersonPlayer(int user1, int user2, int tableNumber) {

        EntityManager em = EntityFactory.getEntityManager();
        if (!em.getTransaction().isActive())
            em.getTransaction().begin();
        Person p = searchPersonID(user1);
        Person p2 = searchPersonID(user2);

        if (p2 != null && p != null) {
            PlayTable t = new PlayTable(p, p2, LocalDateTime.now(), tableNumber);
            em.persist(t);
            em.getTransaction().commit();
            LocalTime now = LocalTime.now();
//            startTimes.put(id, now);
            return true;
        } else
            return false;
    }

    public static List<Person> getPersons() {
        EntityManager em = EntityFactory.getEntityManager();
        return em.createQuery("SELECT p FROM Person p").getResultList();
    }

    public static void registerPerson(Person person) {
        EntityManager entityManager = EntityFactory.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(person);
        entityManager.getTransaction().commit();
    }

    public static void close() {
        EntityManager em = EntityFactory.getEntityManager();
        em.close();
        if (EntityFactory.getEntityManagerFactory().isOpen())
            EntityFactory.getEntityManagerFactory().close();
    }

    public static PlayTable getOnPlayPerson(Person person) {

        EntityManager em = EntityFactory.getEntityManager();
        return (PlayTable) em.createQuery("SELECT t FROM PlayTable t WHERE t.person=:person OR t.person2=:person2")
                .setParameter("person", person)
                .setParameter("person2", person)
                .getSingleResult();

    }

    public static List<LogTable> getTenLastPersonPlayed(Person person) {

        EntityManager em = EntityFactory.getEntityManager();
        return em.createQuery("SELECT t FROM LogTable t WHERE t.person=:person OR t.person2=:person2 ORDER BY t.startTime")
                .setParameter("person", person)
                .setParameter("person2", person)
                .setMaxResults(10)
                .getResultList();
    }

    public static long getTotalTime(Person person) {

        EntityManager em = EntityFactory.getEntityManager();
        List<LogTable> logs = em.createQuery("SELECT t FROM LogTable t WHERE t.person=:person OR t.person2=:person2 ORDER BY t.startTime")
                .setParameter("person", person)
                .setParameter("person2", person)
                .getResultList();

        long totalTime = 0;
        for (LogTable log : logs) {
            Date time = new Date((log.getEndTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()) - log.getStartTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
            totalTime += time.getTime();
        }

        return totalTime;
    }
}
