package DataBase;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static javax.persistence.Persistence.createEntityManagerFactory;

/**
 * Created by Trick on 5/24/2017.
 */
public class EntityFactory {
//    public static String key = "Bar12345Bar12345";
    private static EntityManager entityManager;
    private static EntityManagerFactory entityManagerFactory;

    static {
        EntityManagerFactory entityManagerFactory =
                createEntityManagerFactory("ir.amirmhd.java.jpa");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityFactory.setEntityManager(entityManager);
        EntityFactory.setEntityManagerFactory(entityManagerFactory);
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public static void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        EntityFactory.entityManagerFactory = entityManagerFactory;
    }

    public static void setEntityManager(EntityManager entityManager) {
        EntityFactory.entityManager = entityManager;
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }


}
