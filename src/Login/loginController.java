package Login;

import Main.Main;
import Utils.Utils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;

import static Utils.Resources.*;


public class loginController {
    @FXML
    public TextField userName;

    @FXML
    public PasswordField password;

    @FXML
    public Label status;

    public void onSubmit() {
        String pass = EMPTY_MESSAGE;
        String user = EMPTY_MESSAGE;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(USERS_FILE)));
            user = reader.readLine();
            pass = reader.readLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (userName.getText().equals(user) && password.getText().equals(pass)) {

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource(SHOW_FXML));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage stage = (Stage) userName.getScene().getWindow();
            Scene scene = new Scene(root, SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD);
            stage.setScene(scene);
            stage.show();

        } else
            status.setText(ERROR_MESSAGE);
    }

    public void clear() {
        userName.setText(EMPTY_MESSAGE);
        status.setText(EMPTY_MESSAGE);
        password.setText(EMPTY_MESSAGE);
    }

    public void clearPass() {
        status.setText(EMPTY_MESSAGE);
        password.setText(EMPTY_MESSAGE);
    }

    public void onClose() {
        Utils.closeStage(userName,true);
    }
}
