package DeskStatus;

import Data.LogTable;
import Data.Person;
import Data.PlayTable;
import DataBase.DataBaseHepler;
import Show.showController;
import Utils.Utils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static Utils.Utils.showCantFindUserID;

public class deskStatusController {

    @FXML
    Button startOnePerson;

    @FXML
    Button startTwoPerson;

    @FXML
    Label userStartTime;

    @FXML
    Label user1Coin;

    @FXML
    Label user2Coin;

    @FXML
    Label user2CoinLabel;

    @FXML
    Label user2;

    @FXML
    TextField user1Code;

    @FXML
    TextField user2Code;

    int user1;

    public static int id;
    private static List<Integer> multiPlayer = new ArrayList<>();

    public static void setId(String id1) {
        id = Integer.parseInt(Utils.getID(id1));
    }

    private showController mShowcontroller;

    public void setmShowcontroller(Show.showController mShowcontroller) {
        this.mShowcontroller = mShowcontroller;
    }


    public void onePersonStart() {

        Stage stage = (Stage) startOnePerson.getScene().getWindow();
        if (DataBaseHepler.createOnePersonPlayer(Integer.parseInt(user1Code.getText().trim()), id)) {
            stage.close();
            if (mShowcontroller != null)
                mShowcontroller.setButtonStyle(true, id);

        } else {
            showCantFindUserID();
            stage.close();
        }
    }

    public void twoPersonStart() {

        Stage stage = (Stage) startTwoPerson.getScene().getWindow();
        if (DataBaseHepler.createTwoPersonPlayer(user1, Integer.parseInt(user2Code.getText().trim()), id)) {
            multiPlayer.add(id);
            stage.close();
            if (mShowcontroller != null)
                mShowcontroller.setButtonStyle(true, id);

        } else
            showCantFindUserID();
    }

    public void setStatus(String id) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime now = LocalTime.now();
        user1Coin.setText(Integer.toString(now.toSecondOfDay()));
        userStartTime.setText(dtf.format(now));
        boolean multi = multiPlayer.contains(id);
        if (!multi) {
            user2.setStyle("-fx-background-color: cyan");
            user2Coin.setStyle("-fx-text-fill: cyan;");
            user2CoinLabel.setStyle("-fx-text-fill: cyan;");
        }
        setId(id);
    }

    public void nextPerson() throws IOException {

        Person p = DataBaseHepler.searchPersonID(Integer.parseInt(user1Code.getText().trim()));
        if (p != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/DeskStatus/deskOpenStatus2.fxml"));
            Parent root = loader.load();
            deskStatusController controller = loader.<deskStatusController>getController();
            controller.setmShowcontroller(mShowcontroller);
            controller.user1 = p.getCode();
            Stage stage = (Stage) startOnePerson.getScene().getWindow();
            Scene scene = new Scene(root, 500, 300);
            stage.setScene(scene);
            stage.show();
        } else {
            showCantFindUserID();
        }

    }

    public void closePlay() {
        /*EntityManager em = EntityFactory.getEntityManager();
        EntityTransaction tr = em.getTransaction();
        if (!tr.isActive())
            tr.begin();
        PlayTable t = (PlayTable) em.createQuery("SELECT t FROM PlayTable t where t.tableNumber=:tableId")
                .setParameter("tableId", id).getSingleResult();
        t.setEndTime(LocalDateTime.now());
        em.persist(t);
        em.getTransaction().commit();
//        startTimes.remove(id);*/

        if (mShowcontroller != null)
            mShowcontroller.setButtonStyle(false, id);
        PlayTable t = DataBaseHepler.deletePlayTable(id);
        LogTable table = new LogTable(t);
        table.setEndTime(LocalDateTime.now());
        DataBaseHepler.createLog(table);

        if (multiPlayer.contains(id))
            multiPlayer.remove(new Integer(id));
        Stage stage = (Stage) user1Coin.getScene().getWindow();
        stage.close();
    }

}
