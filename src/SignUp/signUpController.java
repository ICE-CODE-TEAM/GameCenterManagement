package SignUp;

import Data.Person;
import DataBase.DataBaseHepler;
import Utils.Resources;
import Utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class signUpController {

    @FXML
    Button close;
    @FXML
    TextField name;
    @FXML
    TextField lastName;
    @FXML
    TextField birthDate;
    @FXML
    TextField phone;
    @FXML
    TextField idCode;

    public void close() {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }

    public void signUp(ActionEvent event) {

        if (!validateFields())
            return;

        Person p = new Person(name.getText() + lastName.getText(), idCode.getText()
                , birthDate.getText(), phone.getText());
        DataBaseHepler.registerPerson(p);
        close();
    }

    public boolean validateFields() {
        StringBuilder builder = new StringBuilder();

        if (name.getText().isEmpty())
            builder.append(Resources.EMPTYNAME_MESSAGE + "\n");

        if (lastName.getText().isEmpty())
            builder.append(Resources.EMPTYLASTNAME_MESSAGE + "\n");

        if (phone.getText().isEmpty())
            builder.append(Resources.EMPTYMOBILE_MESSAGE + "\n");

        if (idCode.getText().isEmpty())
            builder.append(Resources.EMPTYIdCard_MESSAGE + "\n");

        if (!builder.toString().isEmpty()) {
            Utils.showAlert(Resources.REGISTERFAIL_TITLE, builder.toString());
            return false;
        }

        return true;
    }
}
