package Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Person implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int code;
    private String name;
    private String idCode;
    private double coin;
    private String birthDate;
    private String joinDate;
    public String number;
    public boolean vip;

    public Person() {
    }

    public Person(String name, String idCode, String birthDate, String number) {
        this.name = name;
        this.idCode = idCode;
        this.birthDate = birthDate;
        this.joinDate = LocalDateTime.now().toString();
        this.number = number;
    }

//    public void addTable(Table table){
//        tables.add(table);
//    }



    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public double getCoin() {
        return coin;
    }

    public void setCoin(double coin) {
        this.coin = coin;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    @Override
    public String toString() {
        return "Person{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", idCode=" + idCode +
                ", coin=" + coin +
                ", birthDate='" + birthDate + '\'' +
                ", number=" + number +
                ", vip=" + vip +
                '}';
    }
}
