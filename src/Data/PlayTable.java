package Data;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class PlayTable implements Serializable {
    @Id
    @NotNull
    @GeneratedValue
    long id;
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    Person person2;
    @NotNull
    LocalDateTime startTime;
    @Nullable
    LocalDateTime endTime;
    @NotNull
    int tableNumber;

    public PlayTable() {
    }

    public PlayTable(Person person, LocalDateTime startTime, int tableNumber) {
        this.person = person;
        this.startTime = startTime;
        this.tableNumber = tableNumber;
    }
    public PlayTable(Person person, Person person2, LocalDateTime startTime, int tableNumber) {
        this.person = person;
        this.person2 = person2;
        this.startTime = startTime;
        this.tableNumber = tableNumber;
    }

    public PlayTable(Person person, int tableNumber) {
        this.person = person;
        this.tableNumber = tableNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person p1) {
        this.person = p1;
    }

    public Person getPerson2() {
        return person2;
    }

    public void setPerson2(Person p2) {
        this.person2 = p2;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }
}
