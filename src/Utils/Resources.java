package Utils;

public class Resources {

    public static final String AppName_TITLE = "Game Center";
    public static final String DESKSTATUS_TITLE = "Starting";
    public static final String LIST_TITLE = "List";
    public static final String SIGNUP_TITLE = "signUp";
    public static final String USERNOTFIND_TITLE = "Cant find User ID";
    public static final String REGISTERFAIL_TITLE = "Please fill emtpy fileds first";
    public static final String DETAIL_TITLE = "DETAIL";

    public static final int SCREEN_WIDTH_HD = 1360;
    public static final int SCREEN_HEIGHT_HD = 760;

    public static final String SHOW_FXML = "/Show/show.fxml";
    public static final String FIRSTPAGE_FXML = "/FirstPage/FirstPage.fxml";
    public static final String DESKSTATUS_OPEN_FXML = "/DeskStatus/deskOpenStatus.fxml";
    public static final String DESKSTATUS_CLOSE_FXML = "/DeskStatus/deskCloseStatus.fxml";
    public static final String LIST_FXML = "/List/List.fxml";
    public static final String SIGNUP_FXML = "/SignUp/signUp.fxml";
    public static final String DETAIL_FXML = "/Details/details.fxml";

    public static final String USERS_FILE = "start.txt";

    public static final String BKOOD_FONT = "/Resources/Font/BKoodkBd.ttf";
    public static final int BKOOD_FONT_SIZE = 14;

    public static final String BTITRBD_FONT = "/Resources/Font/BTitrBd.ttf";
    public static final int BTITRBD_FONT_SIZE = 24;

    public static final String ERROR_MESSAGE = "خطا";
    public static final String EMPTY_MESSAGE ="";
    public static final String USERNOTFIND_MESSAGE ="Please check spelling or Register it!";
    public static final String EMPTYNAME_MESSAGE ="Name Filed is empty!";
    public static final String EMPTYLASTNAME_MESSAGE ="Last Name Filed is empty!";
    public static final String EMPTYMOBILE_MESSAGE ="Mobile Number Filed is empty!";
    public static final String EMPTYIdCard_MESSAGE ="ID Card Filed is empty!";

}
