package Utils;

import DataBase.DataBaseHepler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Control;
import javafx.stage.Stage;

import static Utils.Resources.USERNOTFIND_MESSAGE;
import static Utils.Resources.USERNOTFIND_TITLE;

public class Utils {

    public static String getID(String rawID) {
        return rawID.replace("_", "");
    }

    public static void showCantFindUserID() {
        showAlert(USERNOTFIND_TITLE,USERNOTFIND_MESSAGE);
    }

    public static void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
//        alert.getButtonTypes().remove(0);.
//        alert.getButtonTypes().add(new ButtonType("بلی"));
        alert.showAndWait();
    }

    public static void closeStage(Control object,boolean isPrimary){
        Stage stage = (Stage) object.getScene().getWindow();
        stage.close();

        if (isPrimary)
            DataBaseHepler.close();
    }
}
