package FirstPage;

import DataBase.EntityFactory;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

import static Utils.Resources.SCREEN_HEIGHT_HD;
import static Utils.Resources.SCREEN_WIDTH_HD;

public class firstPageController {
    @FXML
    ImageView logo;

    @FXML
    ProgressBar pb;

    firstPageController c;

    boolean progress = true;

    public firstPageController() {
        c = this;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    loading();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onHover() {
        logo.setStyle("-fx-effect: dropshadow(gaussian, lime, 1, 1.0, 1, 1)");
    }

    public void onHoverExit() {
        logo.setStyle("");
    }

    public void loading() throws InterruptedException {
        if (progress) {
            progress = false;
            new Thread() {
                @Override
                public void run() {
                    for (int i = 1; i <= 100; i++) {
                        pb.setProgress((float) i / 100);
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/Login/login.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Stage stage = (Stage) pb.getScene().getWindow();
//                            Scene scene = new Scene(root,500,300);
                            Scene scene = new Scene(root, SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD);
                            stage.setScene(scene);
                            stage.show();
                        }
                    });

                }
            }.start();
            new Thread() {
                @Override
                public void run() {
                    EntityFactory.getEntityManager();
                }
            }.start();


        }
    }
}
